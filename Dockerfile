FROM python:3.7
ENV PYTHONUNBUFFERED 1
WORKDIR /app
COPY . /app/
RUN pip install -r requirements.txt
RUN ["python", "-c", "import nltk; nltk.download('punkt'); nltk.download('stopwords');  nltk.download('wordnet')"]
COPY settings.ini.prod /app/settings.ini
CMD ["python", "src/main.py"]