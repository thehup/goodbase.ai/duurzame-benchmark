from setuptools import setup

setup(
    name='hupml',
    version='0.1',
    packages=['hupml'],
    install_requires=[
        'numpy',
        'pandas',
        'sqlalchemy',
        'pyyaml'
    ],
)
