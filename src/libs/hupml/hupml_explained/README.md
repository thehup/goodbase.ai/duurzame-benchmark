# HupML _Explained_

This folder is dedicated to explain how the hupml library works. Examples and perhaps documentation can be added to this folder. 

### Pandas inheritance
Open `hupml_explained.ipynb` to see what choices were made for Pandas inheritance. All custom methods that are going to be added, must be very generic. This makes it useful in next projects and new consultants can easily start using and learning the custom functions.