from joblib import dump, load

from .load_data import merge_data
from .preprocessor import clean_data
from .check import *


def start_classify(brand, brands_dict, html_dict):
    '''Starts classification for new input, overwrites existing json file if we
    decide to post the prediction. Also, new data is inserted into existing SQL table

    Parameters
    ----------
    brand : str
        brand name in string format
    brands_dict : dict
        dictionary containing brands and corresponding values
    html_dict : dict
        dictionary containing IDs and corresponding html pages
    brands_dict : dict
        emporary dictionary containing new brand
    html_dict : dict
        temporary dictionary containing IDs and corresponding html pages

    Returns
    -------

    '''

    # create training and test data 
    # X_train, y_train, _ = merge_data(brands_dict, html_dict) #todo: load brands_dict and html_dict, only needed for 'check certainty'
    X, y, _ = merge_data(brands_dict, html_dict, skiplabel=False) #TODO: merge_data werkt niet meer

    # pre-process data #todo: this is always False? Delete?
    args_preprocess = False
    if args_preprocess:

        corpus_test, corpus_normalized_test = clean_data(X['content'], stem=False, lem=True)
        X['content_raw'] = corpus_test
        X['content'] = corpus_normalized_test

        # choose model
        model_name = 'classifier/saved_models/svc_model_pp_lem.sav'

    else:
        X_train['content_raw'] = X_train['content']
        X['content_raw'] = X['content']

        # choose model
        model_name = 'classifier/saved_models/svc_model.sav'

    # loading model
    classifier = load(model_name)

    # make prediction
    y_guess = classifier.predict(X)
    y_proba = classifier.predict_proba(X)
    y_proba = y_proba[:, 1]

    X['predict_class'] = y_guess
    X['predict_proba'] = y_proba

    # check certainty #todo: make_decision doesn't yet work
    # X = make_decision(X_train, X)

    # add predictions for new brand to existing database
    brands_dict[brand]['predict_class'] = X[X['brand'] == brand]['predict_class'].item()
    brands_dict[brand]['predict_proba'] = X[X['brand'] == brand]['predict_proba'].item()
    # brands_dict[brand]['predict_neighbour'] = X[X['brand'] == brand]['predict_neighbour'].item()
    # brands_dict[brand]['predict_closeclass'] = X[X['brand'] == brand]['predict_closeclass'].item()
    # brands_dict[brand]['predict_n_std'] = X[X['brand'] == brand]['predict_n_std'].item()
    # brands_dict[brand]['post_prediction'] = X[X['brand'] == brand]['post_prediction'].item()

    return brands_dict, html_dict