#!/usr/bin/python3
# Leon Graumans
# 29-11-2019
# Solid Professionals
# Project Duurzame Benchmark / Sustainability Benchmarking
#
# script for choosing labels


def get_label(values):
    '''Picks Sustainability label (SUS/NOT) from 'label_gold' column

    Parameters
    ----------
    values : dictionary
        Dictionary containing information of each brand

    Returns
    -------
    label
        a string representing a classification label
    None
        a Nonetype if no label is found
    '''

    if not 'post_prediction' in values:
        return None
    elif str(values['label_gold']) != '':
        return str(values['label_gold'])
    else:
        return None
