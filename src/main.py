import sys
import requests
import time
import json
from urllib import parse

from scraper_pipeline import ScraperPipeline, NoWebsiteFoundError
from classifier_pipeline import ClassifierPipeline
import pandas as pd
from configparser import ConfigParser


sys.path.insert(0, 'src/classifier')
config = ConfigParser()
config.read('settings.ini')

api_url = config['general']['api_url']
scraper_settings = [
    {'check_url': {}},
    {'set_urls_equal_to_url': {}},
    {'find_urls': {}},
    {'find_urls': {}},
    {'select_important_urls': {}},
    {'scrape_urls': {}},
    {'prepare_request': {}},
]
classifier_settings = [
    {'load_model': {}},
    {'get_corpus': {}},
    {'preprocess_text': {'stem': True, 'lem': True}},
    {'predict': {}},
]
headers = {'Authorization': f'Token {config["general"]["token"]}'}


def update_goodbase(reply):
    url = f'{api_url}/api/brands/{reply["id"]}/'
    del reply['urls']
    print(url, reply)
    try:
        requests.put(url=url, data=reply, headers=headers)
    except (requests.exceptions.Timeout, requests.exceptions.TooManyRedirects,
            requests.exceptions.RequestException) as e:
        print(e)
        pass


while True:
    # Send get request to Goodbase API to get one unprocessed brand
    try:
        response = requests.get(f'{api_url}/api/brands/?predict_class=PEN&limit=1', headers=headers)
        response = response.json()['results'][0]
        brand_id = response['id']
        brand = response['brand_name']
        response = requests.get(f'{api_url}/api/brands/{brand_id}/', headers=headers).json()
        print(brand, brand_id)
        url = response['url']
    except (requests.exceptions.Timeout, requests.exceptions.TooManyRedirects, requests.exceptions.RequestException,
            IndexError, KeyError, json.decoder.JSONDecodeError) as e:
        print(e)
        time.sleep(300)
        continue

    if len(response['urls']) == 0:  # No urls are scraped yet for this brand
        p = ScraperPipeline(df=pd.DataFrame(), method_settings=scraper_settings, id=brand_id, brand=brand, url=url)
        try:
            p.run()
        except NoWebsiteFoundError:
            response['predict_class'] = 'WEB'
            print("No Website")
            update_goodbase(response)
            continue

        # Send post request to Goodbsae API to add results from scraping
        url = f'{api_url}/api/urls/'
        for obj in p.post:
            try:
                requests.post(url, data=obj, headers=headers)
            except (requests.exceptions.Timeout, requests.exceptions.TooManyRedirects,
                    requests.exceptions.RequestException) as e:
                failed_url = obj['url']
                print(f"POST request for {p.brand}, {failed_url} failed")
        urls = p.post
    else:
        print("Skip scraping")
        urls = response['urls']

    # Start classifying
    c = ClassifierPipeline(df=pd.DataFrame(), method_settings=classifier_settings, brand=brand, url=url, id=id,
                           html=urls)
    c.run()

    # Send put request to Goodbase API to update 'processed' state of brand
    response['predict_class'] = c.predict_class
    response['predict_proba'] = c.predict_proba
    update_goodbase(response)

    # with open('output.txt', 'a') as f:
    #     f.write(f"{brand} {c.predict_class} {c.predict_proba}\n")
