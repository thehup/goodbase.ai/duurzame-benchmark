import csv
import json
import random
import requests

from hupml import PipelineBase
from typing import List
import pandas as pd
from scraper.util import *
from googlesearch import search
from bs4 import BeautifulSoup
from urllib.parse import quote


class NoWebsiteFoundError(Exception):
    pass


def write_html(content, url):
    filename = f"src/data/html/{quote(url, safe='')}"
    if len(filename) > 128:
        filename = filename[:128]
    with open(filename, 'wb') as f:
        f.write(content)


class ScraperPipeline(PipelineBase):

    def __init__(self, df: pd.DataFrame, method_settings: List, id: int, brand: str, url: str):
        self.id = id
        self.brand = brand
        self.url = url
        self.urls = []
        self.important_urls = []
        self.product_urls = []
        self.html = {}
        self.post = []
        super().__init__(df, method_settings)

    def handle_nans(self):
        pass

    def check_url(self):
        """
        Checks if url is empty. If so, try to find the url using Google API
        """

        if self.url is None or self.url == '':
            self.google_url()

    def google_url(self):
        # todo: Pipeline should stop if no homepage can be found. How to implement this?
        """
        Retrieves homepage of brand using Google search
        """

        query = self.brand + ' clothing'

        try:
            for page in search(query, tld='com', lang='en', num=10, stop=6, pause=3):
                parsed_url = urlparse.urlparse(page)
                clean_url = parsed_url.scheme + '://' + parsed_url.netloc
                clean_brand = re.sub('[^a-zA-Z0-9]', '', self.brand)
                # check if brandname is in url
                if clean_brand in parsed_url.netloc:
                    self.url = clean_url
                    break
        except Exception as e:
            print(e)
            raise NoWebsiteFoundError()

    def set_urls_equal_to_url(self):
        """
        Appends homepage to self.urls as initialization for url search
        """
        self.urls.append(self.url)
        if len(self.urls) == 0:
            raise NoWebsiteFoundError()

    def find_urls(self):
        # todo: set limit on max amount of urls
        """
        Finds all hrefs on pages in self.urls and adds them to self.urls
        """

        found_urls = []
        for url in self.urls:

            response = get_response(url)
            if response is None:
                continue

            soup = BeautifulSoup(response.content, 'html.parser')

            for idx, tag in enumerate(soup.find_all(href=True)):

                if ignore_pages(tag['href']):
                    continue

                found_url = fix_url(url, tag['href'])
                found_url_domain = urlparse.urlparse(found_url).netloc.strip('www.')
                href_domain = urlparse.urlparse(url).netloc.strip('www.')

                if found_url_domain.split('.')[0] != href_domain.split('.')[0]:
                    continue

                found_urls.append(found_url)

        self.urls += found_urls
        self.urls = list(set(self.urls))

    def select_important_urls(self):
        # todo: This doesn't seem to work well. Try 'costes' -> there is only one important url and it is not good.
        # todo: I think much of this code is redundant and not very readable. Should edit and simplify.
        # I don't understand what he tries to do with the language tags. He removes them, but in the end the full
        # link is included anyways.
        #
        # Furthermore, he tries to split the product pages but I think this doesn't work
        # very well. Why parse product pages if you include them in the end anyways?
        #
        # Now all links are checked if they are important and 10 are chosen randomly. However, the order was random
        # from the start. So why parse all instead of stopping at 10?
        #
        # A better approach might be to loop over each url and if contains a 'about' keyword immediately
        # include it until we have 10 links. And then, if there are still slots left, random choose the rest.
        """
        Selects 10 important URLs for given brand. Uses keywords to do so. If less
        than 10 URLs are found, the remainder will be randomly picked from a collection
        of URLs (excluding product pages and account pages)
        """

        # import csv with ietf language tags
        ietf_lang = 'src/scraper/data/ietf-language-tags_csv.csv'
        lang_tags = dict()
        with open(ietf_lang) as f:
            reader = csv.reader(f, delimiter=',')
            for line in reader:
                lang_tags[line[0].lower()] = line[1]

        # import json file containing lists of keywords
        page_dict = 'src/scraper/data/page_dictionary.txt'
        with open(page_dict) as json_file:
            pages = json.load(json_file)

        list_of_paths = []
        for url in self.urls:
            parsed_url = urlparse.urlparse(url)
            if parsed_url.path.lower().startswith(('/wp-json', '/../..', '\\', '/mailt')):
                continue
            if parsed_url.path.lower().endswith(('.xml', '.oembed', '.atom', '.js', 'json', '.svg')):
                continue

            clean_url = parsed_url.scheme + '://' + parsed_url.netloc + parsed_url.path
            url_parts = ['/' + part for part in parsed_url.path.split('/') if part]
            if not url_parts:
                continue

            # often, urls contain two language tags: /en/en-gb/
            # if so, delete these tags from the url_parts
            # and save first part of path & full path in list_of_paths
            tmp = re.sub('_', '-', url_parts[0].lstrip('/'))
            if tmp in lang_tags:
                if lang_tags[tmp] != 'en':
                    continue
                url_parts = url_parts[1:]  # delete item 0
                if not url_parts:
                    continue
                tmp = re.sub('_', '-', url_parts[0].lstrip('/'))
                if tmp in lang_tags:
                    if lang_tags[tmp] != 'en':
                        continue
                    url_parts = url_parts[1:]  # delete item 0
            if url_parts:
                list_of_paths.append((url_parts, clean_url))

        # create list of all_urls, except for product & accountpages
        # create list of urls that match any of the keywords in aboutpages
        all_urls = []
        important_urls = []
        product_urls = []
        for path in list_of_paths:
            if any(sub_url in path[0] for sub_url in pages['product']):
                product_urls.append(path[1])
                continue
            elif any(sub_url in path[0] for sub_url in pages['account']):
                continue
            all_urls.append(path[1])
            if any(sub_url in path[1] for sub_url in pages['about']):
                important_urls.append(path[1])

        all_urls = list(set(all_urls))
        important_urls = list(set(important_urls))
        product_urls = list(set(product_urls))

        # if length of important_urls is smaller than 10, pick random from all_urls
        # elif length of important_urls is bigger than 10, random remove some urls
        if len(important_urls) < 10:
            remainder = 10 - len(important_urls)
            random.shuffle(all_urls)
            part_urls = all_urls[:remainder]
            important_urls += part_urls
        elif len(important_urls) > 10:
            random.shuffle(important_urls)
            important_urls = important_urls[:10]

        self.important_urls += important_urls
        self.product_urls += product_urls

    def scrape_urls(self):
        # todo: this function still calls a lot of functions from util. Should we move the logic here?
        """
        Scrapes urls in self.important_urls
        """

        html_dict = dict()

        for url in self.important_urls:
            html_dict[url] = {}

            response = get_response(url)
            if response is None:
                continue

            write_html(response.content, url)

            soup = BeautifulSoup(response.content, 'html.parser')
            content = parse_html(soup)
            content_english = translate_to_EN(content)

            html_dict[url] = {
              'html': soup.prettify(),
              'content': content,
              'content_english': content_english
            }

        self.html = html_dict

    def prepare_request(self):
        """
        Prepares object self.post for post request to api
        Object contains all urls in self.urls
        Currently only send original content, not translated to english
        Currently only saves important urls
        """
        for i, url in enumerate(self.important_urls):
            obj = dict()
            obj['brand'] = self.id
            obj['url'] = url
            obj['used_for_prediction'] = True
            try:
                obj['html_content'] = self.html[url]['content']
            except KeyError:
                continue
            self.post.append(obj)
