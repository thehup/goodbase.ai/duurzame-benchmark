import eli5
from hupml import PipelineBase
from typing import List, Dict
import pandas as pd
from joblib import load
import re
import json
import sys
from boilerpy3 import extractors
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer, PorterStemmer


class ClassifierPipeline(PipelineBase):

    def __init__(self, df: pd.DataFrame, method_settings: List, id: int, brand: str, url: str, html: Dict):
        self.id = id
        self.brand = brand
        self.url = url
        self.html = html
        self.corpus = ''
        self.corpus_cleaned = ''
        self.corpus_normalised = ''
        self.classifier = None
        self.predict_class = None
        self.predict_proba = None
        super().__init__(df, method_settings)

    def handle_nans(self):
        pass

    def get_corpus(self):
        for url in self.html:
            self.corpus += url['html_content']
            self.corpus += ' '

    def load_model(self, path: str = 'src/classifier/saved_models/svc_model_pp_lem.sav'):
        # This function loads the model as a Sklearn Pipeline which will execute all of the feature extraction
        # steps automatically. If we want to edit any choices within this feature extraction, we would have to
        # generate a new .sav model. In that case it would probably be better to throw out Leons file and find a
        # simpler way to store the model for ourselves, which uses Jelles pipeline instead of Sklearns.
        """
        Loads a .sav classifier and saves to self.classifier
        :param path: path to file. There are 2 possible models.
        """
        self.classifier = load(path)

    def preprocess_text(self, stem: bool = False, lem: bool = False):
        # todo: Lars: I do not agree with how the data is preprocessed. I propose we rewrite this function completely.
        # - Waarom worden er eerst zinnen gemaakt van de tekst?
        # - Waarom gooit hij elke zin met een stop-woord eruit?
        """Cleans corpus and tokenizes each document

        Parameters
        ----------
        X : list
            List of strings
        stem : bool, optional
            A flag used to stem words in the corpus (default is False)
        lem : bool, optional
             A flag used to lemmatize words in the corpus (default is False)

        Returns
        -------
        corpus
            a list of strings representing all documents, without stemming/lemmatizing
        corpus_normalized
            a list of strings representing all documents, stemmed/lemmatized if flag is provided
        """

        ignore_keywords = ['javascript', 'browser', 'compatible', 'account', 'cookie', 'cookies', 'policy', 'privacy',
                           'terms', 'disclaimer', 'warranty', 'ship', 'shipping', 'return', 'returned', 'exchange',
                           'exchanged', 'please']

        lines = self.corpus.split('.')
        for line in lines:
            line = re.sub(r'\W', ' ', line)  # remove punctuation
            line = re.sub(r'\s+', ' ', line)  # remove whitespaces
            line = re.sub(' \d+', ' ', line)  # remove numbers
            tok = word_tokenize(line)
            tok = [x.lower() for x in tok]
            if any(word in ignore_keywords for word in tok):  # ignore lines that contain certain keywords
                continue

            tok = [word for word in tok if word not in stopwords.words('english')]

            self.corpus_cleaned += ' '.join(tok)
            self.corpus_cleaned += ' '

            if stem:
                tok = [PorterStemmer().stem(token) for token in tok]
            if lem:
                tok = [WordNetLemmatizer().lemmatize(token, pos='v') for token in tok]

            self.corpus_normalised += ' '.join(tok)
            self.corpus_normalised += ' '

    def predict(self):
        """
        Makes prediction on normalised corpus with loaded model.
        The predict method needs to get a dataframe with columns 'brand', 'content_raw' and 'content'.
        """
        x = pd.DataFrame({'brand': self.brand, 'content_raw': self.corpus, 'content': self.corpus_normalised}, index=[0])
        self.predict_class = self.classifier.predict(x)[0]
        print(self.classifier.classes_, self.classifier.predict_proba(x))
        self.predict_proba = self.classifier.predict_proba(x)[0][self.classifier.classes_.tolist().index(self.predict_class)]
        if self.predict_class == 'SUS' and self.predict_proba < 0.79:
            self.predict_class = 'UNK'
        elif self.predict_class == 'NOT' and self.predict_proba < 0.62:
            self.predict_class = 'UNK'

    def explain(self):
        vec = self.classifier[0].transformer_list[0][1][1]
        clf = self.classifier[1]
        x = pd.DataFrame({'brand': self.brand, 'content_raw': self.corpus, 'content': self.corpus_normalised},
                         index=[0])
        print(eli5.explain_prediction(clf, self.classifier[0].transform(x.iloc[0]), vec=vec))
        # print(eli5.format_as_html(eli5.show_weights(clf, vec=vec, top=20)))
        import pdb; pdb.set_trace()
        te = TextExplainer(random_state=42)
        print(self.corpus_normalised, self.classifier.predict_proba)
        x = pd.DataFrame({'brand': self.brand, 'content_raw': self.corpus, 'content': self.corpus_normalised},
                         index=[0])
        te.fit(x, self.classifier.predict_proba)
        te.show_prediction(target_names=self.predict_class)

    # todo: add function make_decision from Leon. Is found in classifier/check.py
    def make_decision(self):
        """
        checks model prediction
        :return:
        """
        pass

    # todo: add 'predict_neighbour', 'predict_closeclass', 'predict_n_std', 'post_prediction'


# sys.path.insert(0, 'src/classifier')
#
# with open('src/html.json', 'r') as fp:
#     html = json.load(fp)
#
# method_settings = [
#     {'load_model': {}},
#     {'get_corpus': {}},
#     {'preprocess_text': {'stem': True, 'lem': True}},
#     {'predict': {}},
#     #{'explain': {}}
# ]
#
# p = ClassifierPipeline(df=pd.DataFrame(), method_settings=method_settings, brand='act', url='', id=1, html=html)
# p.run()
